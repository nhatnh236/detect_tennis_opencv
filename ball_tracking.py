# USAGE
# python ball_tracking.py --video ball_tracking_example.mp4
# python ball_tracking.py
# pylint:disable=E1101

# import the necessary packages
from imutils.video import VideoStream
import numpy as np
import argparse
import cv2
import imutils
import time

# cho phép sử dụng video có sẵn, truyền vào path 
ap = argparse.ArgumentParser()
ap.add_argument("-v", "--video",
	help="path to the (optional) video file")
args = vars(ap.parse_args())

# dinh nghia bien xanh cua bong tennis
greenLower = (29, 86, 6)
greenUpper = (64, 255, 255)

# nếu không dùng video có sẵn thì sử dụng webcam
if not args.get("video", False):
	vs = VideoStream(src=0).start()

# nếu sư dụng video có sẵn thì lấy file videp
else:
	vs = cv2.VideoCapture(args["video"])

# khởi động camera hoặc file video
time.sleep(1.0)

# Vòng lặp xử lý nhận dạng
while True:
	# lấy khung hình hiện tại
	frame = vs.read()

	# xử lý khung hình từ VideoCapture hoặc VideoStream
	frame = frame[1] if args.get("video", False) else frame

	# nếu không lấy được khung hình của video thì thoát quá trình xử lý
	if frame is None:
		break

	# resize lại khung hình, xử lý blur, và chuyển nó và không gian màu HSV
	frame = imutils.resize(frame, width=600)
	blurred = cv2.GaussianBlur(frame, (11, 11), 0)
	hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)

	# đánh dấu màu xanh, sau đó thục hiện một chuỗi hành động để loại bỏ các màu không đúng
	mask = cv2.inRange(hsv, greenLower, greenUpper)
	mask = cv2.erode(mask, None, iterations=2)
	mask = cv2.dilate(mask, None, iterations=2)

	# tìm viền của quả bóng và vẽ nó
	# sau đó vẽ tâm của quả bóng
	cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL,
		cv2.CHAIN_APPROX_SIMPLE)
	cnts = cnts[0] if imutils.is_cv2() else cnts[1]
	center = None

	# chỉ xử lý nếu ít nhất một viền được tìm thấy
	if len(cnts) > 0:
		# c = max(cnts, key=cv2.contourArea)
		# tìm tất cả các viền, sau đó dùng nó để vẽ vòng tròn bao và tâm
		for c in cnts:
			((x, y), radius) = cv2.minEnclosingCircle(c)
			M = cv2.moments(c)
			center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))

			# only proceed if the radius meets a minimum size
			# chỉ xử lý nếu bán kinh là lớn hơn một giá trị được set up
			if radius > 20:
				# vẽ các vòng tròn và tâm trong khung hình
				cv2.circle(frame, (int(x), int(y)), int(radius),
					(0, 255, 255), 2)
				cv2.circle(frame, center, 5, (0, 0, 255), -1)


	# hiện thị các khung hình trên màn hình
	cv2.imshow("Frame", frame)
	cv2.imshow("Mask", mask)
	key = cv2.waitKey(1) & 0xFF

	# nhấn q nếu muốn kết thúc vòng lặp
	if key == ord("q"):
		break

# nếu không sử dụng video có sẵn thì dừng webcam lại
if not args.get("video", False):
	vs.stop()

else:
	vs.release()

# đóng các cửa sổ
cv2.destroyAllWindows()